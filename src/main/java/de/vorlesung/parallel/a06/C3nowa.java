package de.vorlesung.parallel.a06;

import java.awt.Color;
/**
 * Diese Klasse stellt einen Pixel dar. Daf�r wird die Farbintensit�t der 3 Grundfarben gespeichert.
 * Bei der parallelen Version wird diese Klasse zudem zum Synchronisieren benutzt.  
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */
public class C3nowa {
	int r, g, b;
	boolean made;
	/**
	 * Diese Methode ist der contstructor.
	 * 
	 * @param c
	 * 			Farbe des Pixels als Integerwert
	 * 			
	 * @return nichts
	 */
	public C3nowa(int c) {
		Color color = new Color(c);
		this.r = color.getRed();
		this.g = color.getGreen();
		this.b = color.getBlue();
		this.made = false;
	}
	/**
	 * Diese Methode ist der contstructor.
	 * 
	 * @param r
	 * 			Intensit�t der roten Farbe im Pixel
	 * @param g
	 * 			Intensit�t der gelben Farbe im Pixel
	 * @param b	
	 * 			Intensit�t der blauen Farbe im Pixel
	 * @return nichts
	 */
	public C3nowa(int r, int g, int b) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.made = false;
	}
	/**
	 * Diese Methode addiert zwei Pixel miteinander
	 * 
	 * @param o
	 *            Pixel, das aufadiert werden soll
	 * @return nichts
	 */
	synchronized public void add(C3nowa o) {
		r = r + o.r;
		g = g + o.g;
		b = b + o.b;
	}
	/**
	 * Diese Methode subtrahiert ein fremdes Pixel von diesem und gibt ein neues Pixel mit diesen Werten aus
	 * 
	 * @param o
	 *            Pixel, das abgezogen wird
	 * @return C3nowa
	 * 			  Das subtrahierte Pixel
	 */
	synchronized public C3nowa sub(C3nowa o) {
		return new C3nowa(r - o.r, g - o.g, b - o.b);
	}
	/**
	 * Diese Methode multipliziert einen double Wert mit diesem Pixel und gibt ein neues mit diesen Werten aus
	 * 
	 * @param d
	 *            Multiplikator
	 * @return C3nowa
	 * 			  Pixel nach der Multiplikation
	 */
	synchronized public C3nowa mul(double d) {
		return new C3nowa((int) (d * r), (int) (d * g), (int) (d * b));
	}

	/**
	 * Diese Methode gibt den Unterschied dieses Pixel im Vergleich zu einem anderen aus
	 * 
	 * @param o
	 *            Pixel, das verglichen wird
	 * @return int
	 * 			  Unterschied der beiden Pixel
	 */
	synchronized public int diff(C3nowa o) {
		return Math.abs(r - o.r) + Math.abs(g - o.g) + Math.abs(b - o.b);
	}

	/**
	 * Diese Methode gibt den Farbwert des Pixels in einem Integerwert wieder
	 * 
	 * @return int
	 * 			  Farbwert des Pixels
	 */
	synchronized public int toRGB() {
		return toColor().getRGB();
	}

	/**
	 * Diese Methode gibt ein Color Objekt aus den Werten des Pixels aus
	 * 
	 * @return Color
	 * 			Colorobjekt aus dem Pixel 	 
	 */
	synchronized public Color toColor() {
		return new Color(clamp(r), clamp(g), clamp(b));
	}

	/**
	 * Diese Methode gibt einen integer Wert in einem bestimmten Bereich aus
	 * 
	 * @param c
	 * 			integer Wert
	 * @return int
	 * 			  integer Wert im bestimmten Bereich
	 */
	synchronized public int clamp(int c) {
		return Math.max(0, Math.min(255, c));
	}

	/**
	 * Diese Methode sagt, dass die Methode den Fehlerwert bekommen hat hat und gibt ein notify f�r wartende Threads
	 * Daf�r wird made auf true gesetzt
	 * 
	 */
	synchronized public void setReady() {
		made = true;
		this.notify();
	}
	/**
	 * Diese Methode wartet so lange, bis made true ist
	 * 
	 */
	synchronized public void isReady() {
		if (!made)
			try {
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}
