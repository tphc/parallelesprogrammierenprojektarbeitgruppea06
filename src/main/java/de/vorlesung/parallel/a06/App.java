package de.vorlesung.parallel.a06;

/**
 * Hello world!
 */
public final class App {

    private App() {
    }

    /**
     * Starts the Application
     *
     * @param args The Arguments
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
