package de.vorlesung.parallel.a06.linechanger;

import java.awt.image.BufferedImage;

import de.vorlesung.parallel.a06.C3nowa;
/**
 * Diese Klasse f�hrt die Fehlerdiffusion durch, daf�r wird FloydSteinberg benutzt.
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */
public class LineChangerFloydSteinberg1 implements Runnable {
	// Reihe, in der der Thread beginnt
	private int lineNum;
	// Bild als BufferedImage
	BufferedImage img;
	// Bild als Array
	private C3nowa[][] d;
	// Abmessung des Bildes
	private int w, h;
	// palette
	private C3nowa[] palette;
	
	/**
	 * Diese Methode ist der contstructor.
	 * 
	 * @param img
	 * 			Bild als BufferedImage
	 * @param lineNum
	 * 			Nummer der Bildreihe, die bearbeitet werden soll(von oben)
	 * @param w
	 * 			Breite des Bildes
	 * @param h
	 *          H�he des Bildes
	 * @param d
	 * 			Jedes Pixel des Bildes als C3nowa Objekt
	 * @param palette
	 *            Die Palette, aus der die ähnlichste Farbe gefunden werden
	 *            soll.
	 * @return nichts
	 */
	
	public LineChangerFloydSteinberg1(BufferedImage img, int lineNum,  int w, int h, C3nowa[][] d,
			C3nowa[] palette) {
		this.lineNum = lineNum;
		this.img = img;
		this.d = d;
		this.w = w;
		this.h = h;
		this.palette = palette;
	}

	/**
	 * Diese Methode findet wird ausgef�hrt, wenn das runnable gestartet wird.
	 * Es wird auf einer Reihe des Bildes die Floyd-Steinberg Fehlerdiffusion durchgef�hrt. 
	 * 
	 */
	public void run() {
		// Start des Algorithmus f�r jeden Pixel
		for (int x = 0; x < img.getWidth(); x++) {

			// warten bis Thread arbeiten darf
			d[lineNum][x].isReady();

			// Pixelfarbe auslesen
			C3nowa oldColor = d[lineNum][x];
			// �hnlichsten RGB-Wert aus Palette (s/w) bestimmen
			C3nowa newColor = findClosestPaletteColor(oldColor, palette);
			// neuen RGB-Wert (s/w) schreiben
			img.setRGB(x, lineNum, newColor.toColor().getRGB());
			// Fehler bestimmen: Pixel voneinander abziehen
			C3nowa err = oldColor.sub(newColor);

			// Fehlerdiffusion nach Floyd-Steinberg durchf�hren, wenn
			// Bereich noch im Bild liegt
			if (x + 1 < w)
				d[lineNum][x + 1].add(err.mul(7. / 16));
			if (x - 1 >= 0 && lineNum + 1 < h)
				d[lineNum + 1][x - 1].add(err.mul(3. / 16));
			if (lineNum + 1 < h)
				d[lineNum + 1][x].add(err.mul(5. / 16));
			if (x + 1 < w && lineNum + 1 < h)
				d[lineNum + 1][x + 1].add(err.mul(1. / 16));
			// in Synchronizer schreiben, Signal an andere Threads
			if (x - 2 >= 0 && lineNum + 1 < h) {
				d[lineNum + 1][x - 2].setReady();
			}
			if (x + 1 == w && lineNum + 1 < h) {
				d[lineNum + 1][x - 1].setReady();
				d[lineNum + 1][x].setReady();
			}
		}
	}

	
	/**
	 * Diese Methode findet die ähnlichste Farbe aus der Palette.
	 * 
	 * @param c
	 *            Die Farbe, von der die ähnlichste Farbe gefunden werden soll.
	 * @param palette
	 *            Die Palette, aus der die ähnlichste Farbe gefunden werden
	 *            soll.
	 * @return Die ähnlichste Farbe.
	 */
	private static C3nowa findClosestPaletteColor(C3nowa c, C3nowa[] palette) {
		C3nowa closest = palette[0];

		// Ein Farbwert < 128 -> schwarz, da Grauwerte eines monochromen Bildes
		// immer dieselben Einzel-RGB-Werte haben!
		if (c.toColor().getRed() < 128)
			closest = new C3nowa(0, 0, 0);
		else
			closest = new C3nowa(255, 255, 255);

		return closest;
	}

}
