package de.vorlesung.parallel.a06.errordiffusion;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import de.vorlesung.parallel.a06.C3nowa;
import de.vorlesung.parallel.a06.linechanger.LineChangerFloydSteinberg1;
import de.vorlesung.parallel.a06.linechanger.LineChangerOptionThree1;
import de.vorlesung.parallel.a06.linechanger.LineChangerOptionTwo1;

/**
 * Diese Klasse ist eine parallele Implementiertung der drei
 * Fehlerdiffusionsalgorithmen
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */



public class ErrorDifffusionParallel {
	
	/**
	 * Das ist die main Methode, die die inputOptions Methode aufruft.
	 * 
	 * @param args
	 *            Nicht verwendet.
	 * @return Nichts.
	 * @exception IOException
	 *                Bei einem input Fehler.
	 * @see IOException
	 */
	public static void main(String[] args) throws IOException {

		inputOptions();
	}

	
	/**
	 * Diese Methode ist für die Benutzereingabe zuständig.
	 * 
	 * @return Nichts.
	 * @exception IOException
	 *                Bei einem input Fehler.
	 * @see IOException
	 */
	public static void inputOptions() throws IOException {
		int option;
		int numberOfThreads;
		Scanner scanner = new Scanner(System.in);
		System.out.println(
				"Welche Variante? Bitte Zahl eingeben:\n 1 für Floyd-Steinberg\n 2 für Variante 2\n 3 für Variante 3");
		option = scanner.nextInt();
		System.out.println("Wie viele Threads?");
		numberOfThreads = scanner.nextInt();
		scanner.close();

		runErrorDiffusion("Beispiel_farbig.jpg", option, numberOfThreads);
	}
	
	/**
	 * Diese Methode ruft den Ausgewählten Fehlerdiffusion-Algorithmus auf und erstellt erstellt einen Bildfile.
	 * 
	 * @param inputImageName
	 *            Der Name von dem Bild, auf das der Alogrithmus angewandt
	 *            werden soll.
	 * @param option
	 *            Der Parameter gibt an, welcher Algorithmus verwendet wird.
	 * @param numberOfThreads
	 * 			  Der Praameter gibt an, wie viele Threads verwendet werden sollen.
	 * @return Das bearbeitete Bild.
	 * @exception IOException
	 *                Bei einem input Fehler.
	 * @see IOException
	 */

	public static BufferedImage runErrorDiffusion(String inputImageName, int option, int numberOfThreads) throws IOException {
		final long startTime = System.currentTimeMillis();

		String optionName = "";
		BufferedImage inputImage = ImageIO.read(new File(inputImageName));

		// RGB-Werte, die bei der Fehlerdiffusion verwendet werden (normal:
		C3nowa[] palette = new C3nowa[] { new C3nowa(0, 0, 0), new C3nowa(255, 255, 255) };

		// Abmessungen des Bildes
		int w = inputImage.getWidth();
		int h = inputImage.getHeight();

		// 2-dim. Array f�r Bild-Pixel erstellen
		C3nowa[][] d = new C3nowa[h][w];

		// Jeden RGB-Wert eines Pixels auslesen und im Array speichern
		for (int y = 0; y < h; y++)
			for (int x = 0; x < w; x++)
				d[y][x] = new C3nowa(inputImage.getRGB(x, y));

		// Erste Reihe bereit
		for (int i = 0; i < inputImage.getWidth(); i++) {
			d[0][i].setReady();
		}

		// Threadpool wird erstellt, mit Anzahl von gew�nschten Threads
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);

		// Für jede Reihe ein Runnable erstellen

		switch (option) {
		case 1:
			// Fehlerdiffusion nach Floyd-Steinberg durchf�hren, wenn
			// Bereich noch im Bild liegt
			for (int i = 0; i < h; i++) {
				executor.submit(new LineChangerFloydSteinberg1(inputImage, i, w, h, d, palette));
			}
			optionName = "Floyd-Steinberg";
			break;
		case 2:
			// Fehlerdiffusion Nummer 2 durchf�hren (zeilenweise
			for (int i = 0; i < h; i++) {
				executor.submit(new LineChangerOptionTwo1(inputImage, i, w, h, d, palette));
			}
			optionName = "OptionTwo";
			break;
		case 3:
			// Fehlerdiffusion Nummer 3 durchführen (zeilenweise)
			for (int i = 0; i < h; i++) {
				executor.submit(new LineChangerOptionThree1(inputImage, i, w, h, d, palette));
			}
			optionName = "OptionThree";
			break;
		default:
			System.out.println("Ungültige Eingabe! Bitte 1, 2 oder 3 eingeben!");
		}

		executor.shutdown();

		try {
			executor.awaitTermination(700000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		final long endTime = System.currentTimeMillis();

		ImageIO.write(inputImage, "jpeg", new File("Abgabe1" + optionName + "th" + numberOfThreads + ".jpg"));

		System.out.println("Fertig! Dauer: " + (endTime - startTime));
		
		return inputImage;
	}

}
