package de.vorlesung.parallel.a06.errordiffusion;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import javax.imageio.ImageIO;

import de.vorlesung.parallel.a06.C3nowa;

/**
 * Diese Klasse ist eine sequentielle Implementiertung der drei
 * Fehlerdiffusionsalgorithmen
 * 
 * @author Timur Cöl (2261896)
 * @author Magnus Hartmann (3608144)
 * @author Tim Tiede (8374531)
 */
public class ErrorDiffusionSequential {
	
	/**
	 * Das ist die main Methode, die die inputOptions Methode aufruft.
	 * 
	 * @param args
	 *            Nicht verwendet.
	 * @return Nichts.
	 * @exception IOException
	 *                Bei einem input Fehler.
	 * @see IOException
	 */
	public static void main(String[] args) throws IOException {
			inputOptions();
	}

	/**
	 * Diese Methode ist für die Benutzereingabe zuständig.
	 * 
	 * @return Nichts.
	 * @exception IOException
	 *                Bei einem input Fehler.
	 * @see IOException
	 */
	public static void inputOptions() throws IOException {
		long zstVorher = System.currentTimeMillis();
		long zstNachher;
		int option = 0;

		Scanner scanner = new Scanner(System.in);

		while (option != 1 || option != 2 || option != 3) {
			System.out
					.println("Welche Variante? Bitte Zahl eingeben:\n 1 für Floyd-Steinberg\n 2 für Variante 2\n 3 für Variante 3");
			option = scanner.nextInt();
		}

		scanner.close();

		final BufferedImage outputImage = runErrorDiffusion(
				"Beispiel_farbig.jpg", option);

		ImageIO.write(outputImage, "jpeg", new File("Beispiel_sequentiell_"
				+ option + ".jpg"));
		zstNachher = System.currentTimeMillis();
		System.out.println("Zeit benötigt: " + ((zstNachher - zstVorher))
				+ " ms");
	}

	/**
	 * Diese Methode ruft den Ausgewählten Fehlerdiffusion-Algorithmus auf.
	 * 
	 * @param inputImageName
	 *            Der Name von dem Bild, auf das der Alogrithmus angewandt
	 *            werden soll.
	 * @param option
	 *            Der Parameter gibt an, welcher Algorithmus verwendet wird.
	 * @return Das bearbeitete Bild.
	 * @exception IOException
	 *                Bei einem input Fehler.
	 * @see IOException
	 */
	public static BufferedImage runErrorDiffusion(String inputImageName,
			int option) throws IOException {

		BufferedImage inputImage = ImageIO.read(new File(inputImageName));
		// RGB-Werte, die bei der Fehlerdiffusion verwendet werden (normal:
		// schwarz = 0,0,0 und weiß = 255,255,255; 128,128,128 = grau für
		// schönere Bilder)
		C3nowa[] palette = new C3nowa[] { new C3nowa(0, 0, 0),
				new C3nowa(255, 255, 255) };

		// Abmessungen des Bildes
		int w = inputImage.getWidth();
		int h = inputImage.getHeight();

		// 2-dim. Array für Bild-Pixel erstellen
		C3nowa[][] d = new C3nowa[h][w];

		// Jeden RGB-Wert eines Pixels auslesen und im Array speichern
		for (int y = 0; y < h; y++)
			for (int x = 0; x < w; x++)
				d[y][x] = new C3nowa(inputImage.getRGB(x, y));

		// Start des Algorithmus für jeden Pixel
		for (int y = 0; y < inputImage.getHeight(); y++) {
			for (int x = 0; x < inputImage.getWidth(); x++) {

				// Pixelfarbe auslesen
				C3nowa oldColor = d[y][x];
				// ähnlichsten RGB-Wert aus Palette (s/w) bestimmen
				C3nowa newColor = findClosestPaletteColor(oldColor, palette);
				// neuen RGB-Wert (s/w) schreiben
				inputImage.setRGB(x, y, newColor.toColor().getRGB());
				// Fehler bestimmen: Pixel voneinander abziehen
				C3nowa err = oldColor.sub(newColor);

				switch (option) {
				case 1:
					// Fehlerdiffusion nach Floyd-Steinberg durchführen, wenn
					// Bereich noch im Bild liegt
					alogrithmFloydSteinberg(x, w, h, y, d, err);
					break;
				case 2:
					// Fehlerdiffusion Nummer 2 durchführen (zeilenweise
					// v.l.n.r.)
					alogrithmNumber2(x, w, h, y, d, err);
					break;
				case 3:
					// Fehlerdiffusion Nummer 3 durchführen (zeilenweise
					// v.l.n.r.)
					alogrithmNumber3(x, w, h, y, d, err);
					break;
				default:
					System.out
							.println("Ungültige Eingabe! Bitte 1, 2 oder 3 eingeben!");
				}
			}
		}

		// bearbeitetes Bild zurückgeben
		return inputImage;
	}

	/**
	 * Diese Methode findet die ähnlichste Farbe aus der Palette.
	 * 
	 * @param c
	 *            Die Farbe, von der die ähnlichste Farbe gefunden werden soll.
	 * @param palette
	 *            Die Palette, aus der die ähnlichste Farbe gefunden werden
	 *            soll.
	 * @return Die ähnlichste Farbe.
	 */
	private static C3nowa findClosestPaletteColor(C3nowa c, C3nowa[] palette) {
		C3nowa closest = palette[0];

		// Ein Farbwert < 128 -> schwarz, da Grauwerte eines monochromen Bildes
		// immer dieselben Einzel-RGB-Werte haben!
		if (c.toColor().getRed() < 128)
			closest = new C3nowa(0, 0, 0);
		else
			closest = new C3nowa(255, 255, 255);

		return closest;
	}

	/**
	 * Floyd Steinberg Algorithmus in Java
	 * 
	 * @param x
	 * 
	 * @param z
	 * 
	 * @param h
	 * 
	 * @param y
	 * 
	 * @param d
	 * 
	 * @param err
	 * 
	 * @return
	 */
	public static void alogrithmFloydSteinberg(int x, int w, int h, int y,
			C3nowa[][] d, C3nowa err) {
		if (x + 1 < w)
			d[y][x + 1].add(err.mul(7. / 16));
		if (x - 1 >= 0 && y + 1 < h)
			d[y + 1][x - 1].add(err.mul(3. / 16));
		if (y + 1 < h)
			d[y + 1][x].add(err.mul(5. / 16));
		if (x + 1 < w && y + 1 < h)
			d[y + 1][x + 1].add(err.mul(1. / 16));
	}

	/**
	 * Der zweite Algorithmus in Java;
	 * 
	 * @param x
	 * 
	 * @param z
	 * 
	 * @param h
	 * 
	 * @param y
	 * 
	 * @param d
	 * 
	 * @param err
	 * 
	 * @return
	 */
	public static void alogrithmNumber2(int x, int w, int h, int y,
			C3nowa[][] d, C3nowa err) {
		if (x + 1 < w)
			d[y][x + 1].add(err.mul(4. / 12));
		if (x + 2 < w)
			d[y][x + 2].add(err.mul(1. / 12));

		if (x - 1 >= 0 && y + 1 < h)
			d[y + 1][x - 1].add(err.mul(1. / 12));
		if (y + 1 < h)
			d[y + 1][x].add(err.mul(4. / 12));
		if (x + 1 < w && y + 1 < h)
			d[y + 1][x + 1].add(err.mul(1. / 12));

		if (y + 2 < h)
			d[y + 2][x].add(err.mul(1. / 12));
	}

	/**
	 * Der zweite Algorithmus in Java;
	 * 
	 * @param x
	 * 
	 * @param z
	 * 
	 * @param h
	 * 
	 * @param y
	 * 
	 * @param d
	 * 
	 * @param err
	 * 
	 * @return
	 */
	public static void alogrithmNumber3(int x, int w, int h, int y,
			C3nowa[][] d, C3nowa err) {
		if (x + 1 < w)
			d[y][x + 1].add(err.mul(8. / 42));
		if (x + 2 < w)
			d[y][x + 2].add(err.mul(4. / 42));

		if (x - 2 >= 0 && y + 1 < h)
			d[y + 1][x - 2].add(err.mul(2. / 42));
		if (x - 1 >= 0 && y + 1 < h)
			d[y + 1][x - 1].add(err.mul(4. / 42));
		if (y + 1 < h)
			d[y + 1][x].add(err.mul(8. / 42));
		if (x + 1 < w && y + 1 < h)
			d[y + 1][x + 1].add(err.mul(4. / 42));
		if (x + 2 < w && y + 1 < h)
			d[y + 1][x + 2].add(err.mul(2. / 42));

		if (x - 2 >= 0 && y + 2 < h)
			d[y + 2][x - 2].add(err.mul(1. / 42));
		if (x - 1 >= 0 && y + 2 < h)
			d[y + 2][x - 1].add(err.mul(2. / 42));
		if (y + 2 < h)
			d[y + 2][x].add(err.mul(4. / 42));
		if (x + 1 < w && y + 2 < h)
			d[y + 2][x + 1].add(err.mul(2. / 42));
		if (x + 2 < w && y + 2 < h)
			d[y + 2][x + 2].add(err.mul(1. / 42));
	}

}