package de.vorlesung.parallel.a06.linechanger;

import java.awt.image.BufferedImage;
import java.io.IOException;

import de.vorlesung.parallel.a06.errordiffusion.ErrorDifffusionParallel;
import de.vorlesung.parallel.a06.errordiffusion.ErrorDiffusionSequential;
import junit.framework.TestCase;

public class LineChangerOptionTwo1Test extends TestCase {

	public final void testRun() throws IOException {

		BufferedImage ditheredSeq = ErrorDiffusionSequential.dithering("Beispiel_farbig.jpg",2);
		BufferedImage ditheredPar = ErrorDifffusionParallel.runErrorDiffusion("Beispiel_farbig.jpg", 2, 2);
				
		for(int y=0;y<ditheredSeq.getHeight();y++){

			for(int x=0;x<ditheredSeq.getWidth();x++){
				if(!(ditheredSeq.getRGB(x, y)==ditheredPar.getRGB(x, y))){
					fail("Sequentielles und Paralleles Bild stimmen nicht überein");
				}
			}
		}
	}

}
